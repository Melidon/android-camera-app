package hu.bme.aut.android.cameralabor

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import hu.bme.aut.android.cameralabor.databinding.ActivityDetailsBinding
import hu.bme.aut.android.cameralabor.model.Rating
import hu.bme.aut.android.cameralabor.network.GalleryInteractor
import okhttp3.ResponseBody
import java.lang.Exception

class DetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailsBinding
    private lateinit var imageID: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        imageID = intent.getStringExtra("imageID")!!

        binding.btnRate.setOnClickListener {

            var rating: Rating? = null

            try {
                rating = Rating(
                    binding.etImage.text.toString(),
                    binding.etUsername.text.toString(),
                    binding.etVote.text.toString().toInt(),
                    binding.etProfessional.text.toString().toBoolean(),
                    binding.etType.text.toString(),
                    binding.etComment.text.toString(),
                )
            } catch (e: Exception) {

            }

            rating?.let {
                val galleryInteractor = GalleryInteractor()

                galleryInteractor.rate(
                    id = imageID,
                    rating = rating,
                    onSuccess = this::uploadSuccess,
                    onError = this::uploadError
                )
            }

        }

    }

    private fun uploadSuccess(responseBody: ResponseBody) {
        Toast.makeText(this, "Successfully rated!", Toast.LENGTH_SHORT).show()
        finish()
    }

    private fun uploadError(e: Throwable) {
        Toast.makeText(this, "Error during rating photo!", Toast.LENGTH_SHORT).show()
        e.printStackTrace()
    }

}