package hu.bme.aut.android.cameralabor.model

import android.os.Parcel
import android.os.Parcelable
import com.squareup.moshi.Json

class Image(
    @Json(name = "_id") val id: String?,
    val name: String?,
    val description: String?,
    val timestamp: Long,
    val url: String?,
    val size: Long,
    val mimetype: String?,
    val encoding: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readLong(),
        parcel.readString(),
        parcel.readLong(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeLong(timestamp)
        parcel.writeString(url)
        parcel.writeLong(size)
        parcel.writeString(mimetype)
        parcel.writeString(encoding)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Image> {
        override fun createFromParcel(parcel: Parcel): Image {
            return Image(parcel)
        }

        override fun newArray(size: Int): Array<Image?> {
            return arrayOfNulls(size)
        }
    }

}