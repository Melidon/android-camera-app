package hu.bme.aut.android.cameralabor.adapter

import android.app.Activity
import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import hu.bme.aut.android.cameralabor.DetailsActivity
import hu.bme.aut.android.cameralabor.databinding.VhImageBinding
import hu.bme.aut.android.cameralabor.model.Image
import hu.bme.aut.android.cameralabor.network.GalleryAPI

class ImagesAdapter(
    private val context: Context,
    private val images: MutableList<Image>,
    private val mainActivity: Activity
) : RecyclerView.Adapter<ImagesAdapter.ViewHolder>() {

    private val layoutInflater = LayoutInflater.from(context)

    init {
        this.images.reverse()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImagesAdapter.ViewHolder {
        return ViewHolder(VhImageBinding.inflate(layoutInflater, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val image = images[position]
        Glide.with(context).load(GalleryAPI.IMAGE_PREFIX_URL + image.url).into(holder.imageView)
        holder.imageView.setOnClickListener{
            val intentDetails = Intent(mainActivity, DetailsActivity::class.java)

            var bundle = Bundle()
            bundle.putString("imageID", image.id)
            intentDetails.putExtras(bundle)

            // Sajna nem működik ez a része, pedig akko még jól is nézett volna ki :-(
            val options = ActivityOptions.makeSceneTransitionAnimation(mainActivity, holder.imageView, "sharedTransition")

            startActivity(mainActivity, intentDetails, options.toBundle())
        }
    }

    override fun getItemCount() = images.size

    class ViewHolder(binding: VhImageBinding) : RecyclerView.ViewHolder(binding.root) {
        val imageView: ImageView = binding.ivImage
    }
}