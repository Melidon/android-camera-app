package hu.bme.aut.android.cameralabor.model

data class Rating(
    var image: String,
    var username: String,
    var vote: Int,
    var professional: Boolean,
    var type: String? = null,
    var comment: String? = null
)